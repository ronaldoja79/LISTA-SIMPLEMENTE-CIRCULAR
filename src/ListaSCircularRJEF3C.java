/*
    LISTA SIMPLEMENTE  CIRCULAR
    ESPINALES FIGUEROA JAVIER RONALDO 3C ESTRUCTURA  DE DATOS 
*/
class CNodo {
    
   // se dal clase CNodo 
	int dato;
        // se crea un tipo de dato entero con variable dato 
	CNodo siguiente;
        // en el tipo CNodo se le da una varibale siguiente 
        
        
	public CNodo()	{
            siguiente = null;
            // se hace el constructor de la clase CNodo y a siguiente leasigna null 
	}
}

class CLista {
    // se hace la clase CLista 
    CNodo cabeza;
    //tipode dato CNodo con una varibale cabeza
    public CLista()	{
            cabeza = null;
            //el constructor de la clase donde cabeza  le asigna null
            
    }

    public void InsertarDato(int dat) {
        // la clase InsertarDato con un tipo de dato entero con varibale dato 
        CNodo NuevoNodo;
        //tipo CNodo se le da una varibale NuevoNodo 
        CNodo antes, luego;
        //se hace un CNodo antes y luego 
        NuevoNodo = new CNodo();
        //sentencia para llamr al objeto de la otra clase 
        NuevoNodo.dato=dat;
        //se llama el tipo de dato de la clase CNodo  
        int ban=0;
        // se hace un tipo de dato entero con una varibale ban que es igual a 0 
        if (cabeza == null){
            // pregunta si cabeza es igual a null 
            cabeza = NuevoNodo;
            //cabeza le asigna NuevoNodo  
            NuevoNodo.siguiente=cabeza;
            //el siguiente de NuevoNodo apunta a cabeza
        }
         else {  if (dat<cabeza.dato) {
             // viene y pregunta si dat si el dato ingresa es menor que cabeza.dato 
                    antes=cabeza;
                    //antes le asigna cabeza 
                    luego=cabeza;
                    //luego le asigna cabeza
                    
                    do{
                        antes=luego;
                        //antes le asigna luego 
                        luego=luego.siguiente;
                        //y luego apuntaria a luego.siguiente 
                    }while(luego!=cabeza);
                    //mientras luego sea diferente o igual a cabeza se repite el ciclo 
                                        
                    antes.siguiente=NuevoNodo;
                    //antes.siguiente apuntarias a NuevoNodo 
                    cabeza = NuevoNodo;
                    //cabeza le asigna NuevoNodo
                    NuevoNodo.siguiente=luego;
                    //NuevoNodo.siguiente apunta a luego 
                }
                else {  antes=cabeza;
                        luego=cabeza;
                      //antes y luego le asigna cabeza  
                        while (ban==0){
                           //ban es igual a 0  
                            if (dat>=luego.dato) {
                                // pregunta si dat es mayor o igual que luego.dato 
                                antes=luego;
                                //antes le asigna luego 
                                luego=luego.siguiente;
                                //luego.siguiente apuntaria a luego 
                            }
                            if (luego==cabeza){
                                //luego es igual a cabeza
                                ban=1;
                                //bandera cambia a 1 
                            }
                            else {
                                    if (dat<luego.dato){
                                        //dato es menor que luego.dato 
                                        ban=1;
                                        //bandera la asgina 1 
                                    }
                            }
                        }
                        antes.siguiente=NuevoNodo;
                        //antes.siguiente apunta a NuevoNodo 
                        NuevoNodo.siguiente=luego;
                        //NuevoNodo.siguiente apunta a luego 
                }
        }
    }

    public void EliminarDato(int dat) {
        // la clase EliminarDato
        CNodo antes,luego;
        // se da un tipode CNodo ates y luego 
        int ban=0;
        if (Vacia()) {
            System.out.print("Lista vacía ");
            // muestra un mensaje que la Lista esta Vacia
        }
        else {  if (dat<cabeza.dato) {
            //pregunta dat es menor que cabeza.dato 
                    System.out.print("dato no existe en la lista ");
                  //muestra un mensaje dato no existe en la lista   
                }
                else {
                        if (dat==cabeza.dato) {
                            if (cabeza==cabeza.siguiente){
                                cabeza=null;
                            }else{
                            antes=cabeza;
                            luego=cabeza;
                            do{
                                antes=luego;
                                luego=luego.siguiente;
                            }while(luego!=cabeza);
                            cabeza=cabeza.siguiente;
                            antes.siguiente=cabeza;
                            }
                        }
                        else {  antes=cabeza;
                                luego=cabeza;
                                while (ban==0) {
                                    if (dat>luego.dato) {
                                        antes=luego;
                                        luego=luego.siguiente;
                                    }
                                    else ban=1;
                                    if (luego==cabeza) {
                                        ban=1;
                                        System.out.print("numero inexistente ");
                                    }
                                    else {
                                            if (luego.dato==dat) 
                                                ban=1;
                                    }
                                }
                                if (luego==cabeza) {
                                    System.out.print("dato no existe en la Lista ");
                                }
                                else {
                                        if (dat==luego.dato) {
                                            antes.siguiente=luego.siguiente;
                                        }
                                        else 
                                            System.out.print("dato no existe en la Lista ");
                                }
                        }
                }
        }
    }

    public boolean Vacia() {
        return(cabeza==null);
        //returna que cabeza es igual a null 
    }

    public void VisualizarDatos() {
        CNodo Temporal;
        // se crea un tipo de dato CNodo con una varibale Temporal 
        Temporal=cabeza;
        //temporal le asigna a cabeza 
        if (!Vacia()) {
            // pregunta si es diferente que vacia 
            do{
                System.out.print(" " + Temporal.dato +" ");
                // muestra un mensaje con la suma de temporal.dato 
                Temporal = Temporal.siguiente;
                //temporal le asigna a temporal.siguiente 
            }while (Temporal!=cabeza);
            System.out.println("");
        }
        else
            System.out.print("Lista vacía");
    }
}

public class ListaSCircularRJEF3C {
    public static void main(String args[]) {
        CLista objLista= new CLista();
        System.out.println("Lista Original:");
        objLista.InsertarDato(9);
        objLista.InsertarDato(20);
        objLista.InsertarDato(2);
        objLista.InsertarDato(6);
        objLista.InsertarDato(7);
        objLista.VisualizarDatos();
        objLista.EliminarDato(2);
        objLista.VisualizarDatos();
    }
    // se ingresa los elementos 
}
